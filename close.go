package utils

import (
	"database/sql"
	"io"
	"os"
)

// CloseDB for secure close sql DB with message error if error
func CloseDB(sql *sql.DB) {
	PrettyPrint(sql.Close())
}

// CloseBody for secure close io ReadCloser with message error if error
func CloseBody(Body io.ReadCloser) {
	ErrorHandler(Body.Close())
}

// CloseFile for secure close os file with message error if error
func CloseFile(file *os.File) {
	ErrorHandler(file.Close())
}
