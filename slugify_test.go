package utils

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSlugify(t *testing.T) {
	t.Run("Test basic slugification", func(t *testing.T) {
		input := "kožušček hello world"
		expected := "kozuscek-hello-world"
		result := Slugify(input)

		assert.Equal(t, expected, result)
	})

	t.Run("Test special characters", func(t *testing.T) {
		input := "hello@world!123"
		expected := "hello-world-123"
		result := Slugify(input)

		assert.Equal(t, expected, result)
	})

	t.Run("Test multiple separators", func(t *testing.T) {
		input := "this---is----a---test"
		expected := "this-is-a-test"
		result := Slugify(input)

		assert.Equal(t, expected, result)
	})

	t.Run("Test leading and trailing separators", func(t *testing.T) {
		input := "---test---"
		expected := "test"
		result := Slugify(input)

		assert.Equal(t, expected, result)
	})

	t.Run("Test mixed case", func(t *testing.T) {
		input := "CamelCaseTest"
		expected := "camelcasetest"
		result := Slugify(input)

		assert.Equal(t, expected, result)
	})
}
