package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsContainInt(t *testing.T) {
	intData := []int{1, 2, 3, 4, 5}

	tests := []struct {
		name     string
		value    int
		data     []int
		expected bool
	}{
		{
			name:     "Contains Int",
			value:    3,
			data:     intData,
			expected: true,
		},
		{
			name:     "Does Not Contain Int",
			value:    6,
			data:     intData,
			expected: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := IsContainInt(test.value, test.data)
			assert.Equal(t, test.expected, result)
		})
	}
}

func TestIsContainUint(t *testing.T) {
	uintData := []uint{1, 2, 3, 4, 5}

	tests := []struct {
		name     string
		value    uint
		data     []uint
		expected bool
	}{
		{
			name:     "Contains Uint",
			value:    3,
			data:     uintData,
			expected: true,
		},
		{
			name:     "Does Not Contain Uint",
			value:    6,
			data:     uintData,
			expected: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := IsContainUint(test.value, test.data)
			assert.Equal(t, test.expected, result)
		})
	}
}

func TestIsContainUint64(t *testing.T) {
	uint64Data := []uint64{1, 2, 3, 4, 5}

	tests := []struct {
		name     string
		value    uint64
		data     []uint64
		expected bool
	}{
		{
			name:     "Contains Uint64",
			value:    3,
			data:     uint64Data,
			expected: true,
		},
		{
			name:     "Does Not Contain Uint64",
			value:    6,
			data:     uint64Data,
			expected: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := IsContainUint64(test.value, test.data)
			assert.Equal(t, test.expected, result)
		})
	}
}

func TestIsContainInt64(t *testing.T) {
	int64Data := []int64{1, 2, 3, 4, 5}

	tests := []struct {
		name     string
		value    int64
		data     []int64
		expected bool
	}{
		{
			name:     "Contains Int64",
			value:    3,
			data:     int64Data,
			expected: true,
		},
		{
			name:     "Does Not Contain Int64",
			value:    6,
			data:     int64Data,
			expected: false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := IsContainInt64(test.value, test.data)
			assert.Equal(t, test.expected, result)
		})
	}
}
