package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEqualString(t *testing.T) {
	t.Run("Test case-insensitive matching", func(t *testing.T) {
		str := "Report Service"
		subStr := "report"
		result := EqualString(str, subStr)

		assert.True(t, result)
	})

	t.Run("Test case-insensitive non-matching", func(t *testing.T) {
		str := "Report Service"
		subStr := "abc"
		result := EqualString(str, subStr)

		assert.False(t, result)
	})
}

func TestStringToUintArr(t *testing.T) {
	t.Run("Test conversion of string array to uint array", func(t *testing.T) {
		input := []string{"1", "2", "3"}
		expected := []uint{1, 2, 3}
		result := StringToUintArr(input)

		assert.Equal(t, expected, result)
	})
}

func TestIsContainString(t *testing.T) {
	t.Run("Test string is contained in array", func(t *testing.T) {
		str := "apple"
		data := []string{"banana", "apple", "orange"}
		result := IsContainString(str, data)

		assert.True(t, result)
	})

	t.Run("Test string is not contained in array", func(t *testing.T) {
		str := "grape"
		data := []string{"banana", "apple", "orange"}
		result := IsContainString(str, data)

		assert.False(t, result)
	})
}
