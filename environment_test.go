package utils

import (
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGetEnvString(t *testing.T) {
	// Set an environment variable
	os.Setenv("TEST_STRING", "hello")

	// Test the function with an existing environment variable
	result := GetEnvString("TEST_STRING", "default")
	assert.Equal(t, "hello", result)

	// Test the function with a non-existing environment variable
	result = GetEnvString("NON_EXISTENT", "default")
	assert.Equal(t, "default", result)

	// Clean up
	os.Unsetenv("TEST_STRING")
}

func TestGetEnvBool(t *testing.T) {
	// Set an environment variable
	os.Setenv("TEST_BOOL", "true")

	// Test the function with an existing environment variable
	result := GetEnvBool("TEST_BOOL", false)
	assert.Equal(t, true, result)

	// Test the function with a non-existing environment variable
	result = GetEnvBool("NON_EXISTENT", true)
	assert.Equal(t, true, result)

	// Clean up
	os.Unsetenv("TEST_BOOL")
}

func TestGetEnvTimeDuration(t *testing.T) {
	// Set an environment variable
	os.Setenv("TEST_DURATION", "30000")

	// Test the function with an existing environment variable
	result := GetEnvTimeDuration("TEST_DURATION", time.Second)
	assert.Equal(t, time.Duration(30000), result)

	// Test the function with a non-existing environment variable
	result = GetEnvTimeDuration("NON_EXISTENT", time.Minute)
	assert.Equal(t, time.Minute, result)

	// Clean up
	os.Unsetenv("TEST_DURATION")
}

func TestGetEnvInt64(t *testing.T) {
	// Set an environment variable
	os.Setenv("TEST_INT64", "42")

	// Test the function with an existing environment variable
	result := GetEnvInt64("TEST_INT64", 0)
	assert.Equal(t, int64(42), result)

	// Test the function with a non-existing environment variable
	result = GetEnvInt64("NON_EXISTENT", 123)
	assert.Equal(t, int64(123), result)

	// Clean up
	os.Unsetenv("TEST_INT64")
}

func TestGetEnvFloat64(t *testing.T) {
	// Set an environment variable
	os.Setenv("TEST_FLOAT64", "3.14")

	// Test the function with an existing environment variable
	result := GetEnvFloat64("TEST_FLOAT64", 0.0)
	assert.Equal(t, 3.14, result)

	// Test the function with a non-existing environment variable
	result = GetEnvFloat64("NON_EXISTENT", 2.71)
	assert.Equal(t, 2.71, result)

	// Clean up
	os.Unsetenv("TEST_FLOAT64")
}
