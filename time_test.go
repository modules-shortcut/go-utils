package utils

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestSplitTime(t *testing.T) {
	t.Run("Test split time from time.Time", func(t *testing.T) {
		currentTime := time.Now()
		futureTime := currentTime.Add(2 * time.Hour).Add(time.Second)
		hours, minutes, seconds, err := SplitTime(futureTime)

		assert.NoError(t, err)
		assert.Equal(t, 2, hours)
		assert.True(t, minutes >= 0 && minutes < 60)
		assert.True(t, seconds >= 0 && seconds < 60)
	})

	t.Run("Test split time from time.Duration", func(t *testing.T) {
		duration := 3*time.Hour + 15*time.Minute + 30*time.Second
		hours, minutes, seconds, err := SplitTime(duration)

		assert.NoError(t, err)
		assert.Equal(t, 3, hours)
		assert.Equal(t, 15, minutes)
		assert.Equal(t, 30, seconds)
	})

	t.Run("Test split time from string duration", func(t *testing.T) {
		stringDuration := "5h30m45s"
		hours, minutes, seconds, err := SplitTime(stringDuration)

		assert.NoError(t, err)
		assert.Equal(t, 5, hours)
		assert.Equal(t, 30, minutes)
		assert.Equal(t, 45, seconds)
	})
}
