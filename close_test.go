package utils

import (
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"io"
	"os"
	"testing"
)

type MockReadCloser struct {
	Data   []byte
	Closed bool
}

func (m *MockReadCloser) Read(p []byte) (n int, err error) {
	if m.Closed {
		return 0, io.EOF
	}
	copy(p, m.Data)
	m.Closed = true
	return len(m.Data), nil
}

func (m *MockReadCloser) Close() error {
	m.Closed = true
	return nil
}

func TestCloseDB(t *testing.T) {
	// Create a mock DB
	mockDB, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("Error creating mock DB: %v", err)
	}

	// Create a GORM DB instance using the mock DB
	_, gormDBErr := gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{})
	if gormDBErr != nil {
		t.Fatalf("Error creating GORM DB instance: %v", gormDBErr)
	}

	// Test the function
	CloseDB(mockDB)

	// Assert that the DB.Close method was called
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestCloseBody(t *testing.T) {
	// Create a mock ReadCloser
	mockBody := &MockReadCloser{Data: []byte("Hello, World!")}

	// Test the function
	CloseBody(mockBody)

	// Assert that the Close method of the mock ReadCloser was called
	assert.True(t, mockBody.Closed)

	// Test closing a closed body
	CloseBody(mockBody)
}

func TestCloseFile(t *testing.T) {
	// Create a mock os.File
	mockFile := &os.File{}

	// Test the function
	CloseFile(mockFile)

	// Assert that the Close() method of the mock os.File was called
	assert.True(t, true) // Replace with appropriate assertion
}
