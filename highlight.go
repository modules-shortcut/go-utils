package utils

import "regexp"

//Highlight is a func for add prefix and suffix for text
func Highlight(str, searchText, prefix, suffix string) string {
	// Create a regular expression pattern to match the search text
	pattern := regexp.MustCompile(`(?i)` + searchText)

	// Find all matches of the pattern in the string
	matches := pattern.FindAllStringIndex(str, -1)

	// Highlight the matches by adding asterisks (*) around them
	highlightedStr := ""
	prevIndex := 0
	for _, match := range matches {
		start := match[0]
		end := match[1]

		// Append the part of the string before the match
		highlightedStr += str[prevIndex:start]

		// Append the highlighted match
		highlightedStr += prefix + str[start:end] + suffix

		prevIndex = end
	}

	// Append the remaining part of the string
	highlightedStr += str[prevIndex:]

	return highlightedStr
}
