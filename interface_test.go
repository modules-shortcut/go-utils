package utils

import (
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestMergeArrayInterfaces(t *testing.T) {
	data1 := []interface{}{1, 2, 3}
	data2 := []interface{}{4, 5, 6}
	data3 := []interface{}{"a", "b", "c"}

	tests := []struct {
		name     string
		input    [][]interface{}
		expected []interface{}
	}{
		{
			name:     "Merge Single Array",
			input:    [][]interface{}{data1},
			expected: data1,
		},
		{
			name:     "Merge Multiple Arrays",
			input:    [][]interface{}{data1, data2, data3},
			expected: append(append(data1, data2...), data3...),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := MergeArrayInterfaces(test.input...)
			assert.True(t, reflect.DeepEqual(test.expected, result))
		})
	}
}
