package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type TestStruct struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

func TestConvert(t *testing.T) {
	t.Run("Test convert map[string]interface{} to struct", func(t *testing.T) {
		source := map[string]interface{}{
			"id":    1,
			"name":  "John",
			"email": "john@example.com",
		}
		var destination TestStruct
		err := Convert(source, &destination)

		assert.NoError(t, err)
		assert.Equal(t, 1, destination.ID)
		assert.Equal(t, "John", destination.Name)
		assert.Equal(t, "john@example.com", destination.Email)
	})

	t.Run("Test convert JSON string to struct", func(t *testing.T) {
		source := `{"id": 2, "name": "Jane", "email": "jane@example.com"}`
		var destination TestStruct
		err := Convert(source, &destination)

		assert.NoError(t, err)
		assert.Equal(t, 2, destination.ID)
		assert.Equal(t, "Jane", destination.Name)
		assert.Equal(t, "jane@example.com", destination.Email)
	})

	t.Run("Test convert array of map[string]interface{} to struct array", func(t *testing.T) {
		source := []map[string]interface{}{
			{"id": 3, "name": "Alice", "email": "alice@example.com"},
			{"id": 4, "name": "Bob", "email": "bob@example.com"},
		}
		var destination []TestStruct
		err := Convert(source, &destination)

		assert.NoError(t, err)
		assert.Len(t, destination, 2)
		assert.Equal(t, 3, destination[0].ID)
		assert.Equal(t, "Alice", destination[0].Name)
		assert.Equal(t, "alice@example.com", destination[0].Email)
		assert.Equal(t, 4, destination[1].ID)
		assert.Equal(t, "Bob", destination[1].Name)
		assert.Equal(t, "bob@example.com", destination[1].Email)
	})
}
