package utils

// Eq utils function
/*
	Eq means equal
	Used to return the true value according to the value you want to compare.
	The parameters consist of 4 data types interface{} where the first and second parameters are the values to be compared
	while for the third value is the value that will be returned when the condition is true and the fourth parameter is for the value when the condition is false

	Example:
	exp1 := Eq("eat", "sleep", 100, 50) // exp1 = 50
	exp2 := Eq("eat", "eat", 100, 50) // exp2 = 100
*/
func Eq(val1, val2, tRes, fRes interface{}) interface{} {
	if val1 == val2 {
		return tRes
	}
	return fRes
}

//Ne func for if not equals
/*
	Ne means Not Equal
	Used to return the true value according to the value you want to compare.
	The parameters consist of 4 data types interface{} where the first and second parameters are the values to be compared
	while for the third value is the value that will be returned when the condition is true and the fourth parameter is for the value when the condition is false

	Example:
	exp1 := Ne("eat", "sleep", 100, 50) // exp1 = 100
	exp2 := Ne("eat", "eat", 100, 50) // exp2 = 50
*/
func Ne(val1, val2, tRes, fRes interface{}) interface{} {
	if val1 != val2 {
		return tRes
	}
	return fRes
}

//Lt func for if not equals
/*
	Lt means Less Than
	Used to return the true value according to the value you want to compare.
	The parameters consist of 4 data types interface{} where the first and second parameters are the values to be compared
	while for the third value is the value that will be returned when the condition is true and the fourth parameter is for the value when the condition is false

	Example:
	exp1 := Lt(99, 100, "yes", "no") // exp1 = yes
	exp2 := Lt(99, 70, "yes", "no) // exp2 = no
*/
func Lt(val1, val2, tRes, fRes interface{}) interface{} {
	switch v := val1.(type) {
	case string:
		switch w := val2.(type) {
		case string:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case int:
		switch w := val2.(type) {
		case int:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case int64:
		switch w := val2.(type) {
		case int64:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case int32:
		switch w := val2.(type) {
		case int32:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case int16:
		switch w := val2.(type) {
		case int16:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case int8:
		switch w := val2.(type) {
		case int8:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case uint:
		switch w := val2.(type) {
		case uint:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case uint64:
		switch w := val2.(type) {
		case uint64:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case uint32:
		switch w := val2.(type) {
		case uint32:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case uint16:
		switch w := val2.(type) {
		case uint16:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case uint8:
		switch w := val2.(type) {
		case uint8:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case float64:
		switch w := val2.(type) {
		case float64:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	case float32:
		switch w := val2.(type) {
		case float32:
			if v < w {
				return tRes
			}
		default:
			return nil
		}
	default:
		return nil
	}
	return fRes
}

//Gt func for if not equals
/*
	Gt means Greater Than
	Used to return the true value according to the value you want to compare.
	The parameters consist of 4 data types interface{} where the first and second parameters are the values to be compared
	while for the third value is the value that will be returned when the condition is true and the fourth parameter is for the value when the condition is false

	Example:
	exp1 := Gt(99, 100, "yes", "no") // exp1 = no
	exp2 := Gt(99, 70, "yes", "no) // exp2 = yes
*/
func Gt(val1, val2, tRes, fRes interface{}) interface{} {
	switch v := val1.(type) {
	case string:
		switch w := val2.(type) {
		case string:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case int:
		switch w := val2.(type) {
		case int:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case int64:
		switch w := val2.(type) {
		case int64:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case int32:
		switch w := val2.(type) {
		case int32:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case int16:
		switch w := val2.(type) {
		case int16:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case int8:
		switch w := val2.(type) {
		case int8:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case uint:
		switch w := val2.(type) {
		case uint:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case uint64:
		switch w := val2.(type) {
		case uint64:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case uint32:
		switch w := val2.(type) {
		case uint32:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case uint16:
		switch w := val2.(type) {
		case uint16:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case uint8:
		switch w := val2.(type) {
		case uint8:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case float64:
		switch w := val2.(type) {
		case float64:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	case float32:
		switch w := val2.(type) {
		case float32:
			if v > w {
				return tRes
			}
		default:
			return nil
		}
	default:
		return nil
	}
	return fRes
}

//Lte func for if not equals
/*
	Lte means Less Than Equal
	Used to return the true value according to the value you want to compare.
	The parameters consist of 4 data types interface{} where the first and second parameters are the values to be compared
	while for the third value is the value that will be returned when the condition is true and the fourth parameter is for the value when the condition is false

	Example:
	exp1 := Lte(99, 100, "yes", "no") // exp1 = yes
	exp2 := Lte(99, 70, "yes", "no) // exp2 = no
	exp3 := Lte(99, 99, "yes", "no) // exp3 = yes
*/
func Lte(val1, val2, tRes, fRes interface{}) interface{} {
	switch v := val1.(type) {
	case string:
		switch w := val2.(type) {
		case string:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case int:
		switch w := val2.(type) {
		case int:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case int64:
		switch w := val2.(type) {
		case int64:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case int32:
		switch w := val2.(type) {
		case int32:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case int16:
		switch w := val2.(type) {
		case int16:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case int8:
		switch w := val2.(type) {
		case int8:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case uint:
		switch w := val2.(type) {
		case uint:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case uint64:
		switch w := val2.(type) {
		case uint64:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case uint32:
		switch w := val2.(type) {
		case uint32:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case uint16:
		switch w := val2.(type) {
		case uint16:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case uint8:
		switch w := val2.(type) {
		case uint8:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case float64:
		switch w := val2.(type) {
		case float64:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	case float32:
		switch w := val2.(type) {
		case float32:
			if v <= w {
				return tRes
			}
		default:
			return nil
		}
	default:
		return nil
	}
	return fRes
}

//Gte func for if not equals
/*
	Gte means Greater Than Equal
	Used to return the true value according to the value you want to compare.
	The parameters consist of 4 data types interface{} where the first and second parameters are the values to be compared
	while for the third value is the value that will be returned when the condition is true and the fourth parameter is for the value when the condition is false

	Example:
	exp1 := Gte(99, 100, "yes", "no") // exp1 = no
	exp2 := Gte(99, 70, "yes", "no) // exp2 = yes
	exp3 := Gte(99, 99, "yes", "no) // exp3 = yes
*/
func Gte(val1, val2, tRes, fRes interface{}) interface{} {
	switch v := val1.(type) {
	case string:
		switch w := val2.(type) {
		case string:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case int:
		switch w := val2.(type) {
		case int:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case int64:
		switch w := val2.(type) {
		case int64:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case int32:
		switch w := val2.(type) {
		case int32:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case int16:
		switch w := val2.(type) {
		case int16:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case int8:
		switch w := val2.(type) {
		case int8:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case uint:
		switch w := val2.(type) {
		case uint:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case uint64:
		switch w := val2.(type) {
		case uint64:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case uint32:
		switch w := val2.(type) {
		case uint32:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case uint16:
		switch w := val2.(type) {
		case uint16:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case uint8:
		switch w := val2.(type) {
		case uint8:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case float64:
		switch w := val2.(type) {
		case float64:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	case float32:
		switch w := val2.(type) {
		case float32:
			if v >= w {
				return tRes
			}
		default:
			return nil
		}
	default:
		return nil
	}
	return fRes
}
