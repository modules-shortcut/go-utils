package utils

import "time"

// WeekStart Get datetime start from week in year
func WeekStart(year, week int) time.Time {
	//year, week := tm.ISOWeek()
	// Start from the middle of the year:
	t := time.Date(year, 7, 1, 0, 0, 0, 0, time.Now().Location())

	// Roll back to Monday:
	if wd := t.Weekday(); wd == time.Sunday {
		t = t.AddDate(0, 0, -6)
	} else {
		t = t.AddDate(0, 0, -int(wd)+1)
	}

	// Difference in weeks:
	_, w := t.ISOWeek()
	t = t.AddDate(0, 0, (week-w)*7)

	return t
}

// WeekStartWithTime Get datetime start from week in year
func WeekStartWithTime(tm time.Time) time.Time {
	return WeekStart(tm.ISOWeek())
}

// WeekRange Get datetime start and end from week in year
func WeekRange(year, week int) (start, end time.Time) {
	start = WeekStart(year, week)
	end = start.AddDate(0, 0, 6)
	return
}

// WeekRangeWithTime Get datetime start and end from week in year
func WeekRangeWithTime(tm time.Time) (start, end time.Time) {
	return WeekRange(tm.ISOWeek())
}

func SplitTime(data interface{}) (hours, minutes, seconds int, err error) {
	var duration, minutesDur, secondsDur time.Duration
	switch v := data.(type) {
	case time.Time:
		duration = v.Sub(time.Now())
		hours = int(duration.Hours())
		minutesDur = duration - (time.Duration(hours) * time.Hour)
		minutes = int(minutesDur / time.Minute)
		secondsDur = duration - (time.Duration(hours) * time.Hour) - (time.Duration(minutes) * time.Minute)
		seconds = int(secondsDur / time.Second)
	case time.Duration:
		duration = v
		hours = int(duration.Hours())
		minutesDur = duration - (time.Duration(hours) * time.Hour)
		minutes = int(minutesDur / time.Minute)
		secondsDur = duration - (time.Duration(hours) * time.Hour) - (time.Duration(minutes) * time.Minute)
		seconds = int(secondsDur / time.Second)
	case string:
		if duration, err = time.ParseDuration(v); err != nil {
			return
		}
		hours = int(duration.Hours())
		minutesDur = duration - (time.Duration(hours) * time.Hour)
		minutes = int(minutesDur / time.Minute)
		secondsDur = duration - (time.Duration(hours) * time.Hour) - (time.Duration(minutes) * time.Minute)
		seconds = int(secondsDur / time.Second)
	default:
		LogPrint(duration, minutesDur, secondsDur)
	}
	return
}
