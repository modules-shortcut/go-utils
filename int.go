package utils

// IsContainInt Check if array string contain int
func IsContainInt(str int, data []int) bool {
	for _, val := range data {
		if val == str {
			return true
		}
	}
	return false
}

// IsContainUint Check if array string contain uint
func IsContainUint(str uint, data []uint) bool {
	for _, val := range data {
		if val == str {
			return true
		}
	}
	return false
}

// IsContainUint64 Check if array string contain uint64
func IsContainUint64(str uint64, data []uint64) bool {
	for _, val := range data {
		if val == str {
			return true
		}
	}
	return false
}

// IsContainInt64 Check if array string contain int64
func IsContainInt64(str int64, data []int64) bool {
	for _, val := range data {
		if val == str {
			return true
		}
	}
	return false
}
