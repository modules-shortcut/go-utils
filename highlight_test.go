package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHighlight(t *testing.T) {
	tests := []struct {
		str        string
		searchText string
		prefix     string
		suffix     string
		expected   string
	}{
		{
			str:        "This is a test string.",
			searchText: "test",
			prefix:     "<b>",
			suffix:     "</b>",
			expected:   "This is a <b>test</b> string.",
		},
		{
			str:        "Highlight this keyword in the text.",
			searchText: "keyword",
			prefix:     "[",
			suffix:     "]",
			expected:   "Highlight this [keyword] in the text.",
		},
		{
			str:        "No matches here.",
			searchText: "test",
			prefix:     "<b>",
			suffix:     "</b>",
			expected:   "No matches here.",
		},
	}

	for _, test := range tests {
		t.Run(test.expected, func(t *testing.T) {
			result := Highlight(test.str, test.searchText, test.prefix, test.suffix)
			assert.Equal(t, test.expected, result)
		})
	}
}
