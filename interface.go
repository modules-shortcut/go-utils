package utils

//MergeArrayInterfaces
//This function used to merge [][]interface{} to []interface{}
//This function is variadic function which use two dimension array of interface{}
func MergeArrayInterfaces(models ...[]interface{}) (res []interface{}) {
	for _, model := range models {
		res = append(res, model...)
	}
	return
}
