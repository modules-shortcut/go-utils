package utils

import (
	"os"
	"strconv"
	"time"
)

// GetEnvString for get env type string
func GetEnvString(key, defaultVal string) string {
	value, exist := os.LookupEnv(key)
	if !exist {
		return defaultVal
	}
	return value
}

// GetEnvBool for get env type bool
func GetEnvBool(key string, val bool) bool {
	value, exist := os.LookupEnv(key)
	if !exist || value == "" {
		return val
	}

	b, err := strconv.ParseBool(value)
	if err != nil {
		return val
	}
	return b
}

// GetEnvTimeDuration for get env variables type time duration in golang
func GetEnvTimeDuration(key string, val time.Duration) time.Duration {
	if os.Getenv(key) == "" {
		return val
	}

	i, _ := strconv.ParseInt(os.Getenv(key), 10, 64)
	return time.Duration(i)
}

// GetEnvInt64 for get env variables type int64 in golang
func GetEnvInt64(key string, val int64) int64 {
	if os.Getenv(key) == "" {
		return val
	}

	i, _ := strconv.ParseInt(os.Getenv(key), 10, 64)
	return i
}

// GetEnvFloat64 for get env variables type int64 in golang
func GetEnvFloat64(key string, val float64) float64 {
	if os.Getenv(key) == "" {
		return val
	}

	i, _ := strconv.ParseFloat(os.Getenv(key), 64)
	return i
}
