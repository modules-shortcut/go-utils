package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestComparisonFunctions(t *testing.T) {
	tests := []struct {
		name     string
		function func(interface{}, interface{}, interface{}, interface{}) interface{}
		val1     interface{}
		val2     interface{}
		tRes     interface{}
		fRes     interface{}
		expected interface{}
	}{
		{
			name:     "Equal Strings",
			function: Eq,
			val1:     "hello",
			val2:     "hello",
			tRes:     "yes",
			fRes:     "no",
			expected: "yes",
		},
		{
			name:     "Not Equal Strings",
			function: Ne,
			val1:     "hello",
			val2:     "world",
			tRes:     "yes",
			fRes:     "no",
			expected: "yes",
		},
		{
			name:     "Less Than Integers",
			function: Lt,
			val1:     10,
			val2:     20,
			tRes:     "less",
			fRes:     "greater",
			expected: "less",
		},
		// Add more test cases for other comparison functions
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := test.function(test.val1, test.val2, test.tRes, test.fRes)
			assert.Equal(t, test.expected, result)
		})
	}
}
