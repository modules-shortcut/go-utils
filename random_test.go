package utils

import (
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestRandomString(t *testing.T) {
	t.Run("Test default case (lower + upper + digit)", func(t *testing.T) {
		length := 10
		result := RandomString(length, Default)

		assert.Equal(t, length, len(result))
		assert.True(t, containsOnly(result, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"))
	})

	t.Run("Test lower case", func(t *testing.T) {
		length := 8
		result := RandomString(length, LowerCase)

		assert.Equal(t, length, len(result))
		assert.True(t, containsOnly(result, "abcdefghijklmnopqrstuvwxyz"))
	})

	t.Run("Test upper case", func(t *testing.T) {
		length := 6
		result := RandomString(length, UpperCase)

		assert.Equal(t, length, len(result))
		assert.True(t, containsOnly(result, "ABCDEFGHIJKLMNOPQRSTUVWXYZ"))
	})

	t.Run("Test digit case", func(t *testing.T) {
		length := 12
		result := RandomString(length, DigitCase)

		assert.Equal(t, length, len(result))
		assert.True(t, containsOnly(result, "0123456789"))
	})

	t.Run("Test symbol case", func(t *testing.T) {
		length := 16
		result := RandomString(length, SymbolCase)

		assert.Equal(t, length, len(result))
		assert.True(t, containsOnly(result, "!@#$%^&-_=+"))
	})

	t.Run("Test alphabet case", func(t *testing.T) {
		length := 20
		result := RandomString(length, Alphabet)

		assert.Equal(t, length, len(result))
		assert.True(t, containsOnly(result, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"))
	})

	t.Run("Test all case", func(t *testing.T) {
		length := 15
		result := RandomString(length, AllCase)

		assert.Equal(t, length, len(result))
		assert.True(t, containsOnly(result, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&-_=+"))
	})
}

func containsOnly(str, validChars string) bool {
	for _, c := range str {
		if !strings.ContainsRune(validChars, c) {
			return false
		}
	}
	return true
}
