package utils

import (
	"crypto/rand"
	"math/big"
)

type kindOfCase string

const (
	LowerCase  kindOfCase = "lower"
	UpperCase  kindOfCase = "upper"
	Alphabet   kindOfCase = "alphabet"
	DigitCase  kindOfCase = "digit"
	SymbolCase kindOfCase = "symbol"
	AllCase    kindOfCase = "all"
	Default    kindOfCase = ""
)

//RandomString is a utils for generate random string with param length of string and type of string
// kind = (lower/upper/digit/symbol/all) or default is lower+upper+digit
func RandomString(length int, kind kindOfCase) string {
	var (
		letters []rune
		b       = make([]rune, length)
		lower   = "abcdefghijklmnopqrstuvwxyz"
		upper   = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		digit   = "0123456789"
		symbol  = "!@#$%^&-_=+"
	)

	switch kind {
	case LowerCase:
		letters = []rune(lower)
	case UpperCase:
		letters = []rune(upper)
	case DigitCase:
		letters = []rune(digit)
	case SymbolCase:
		letters = []rune(symbol)
	case Alphabet:
		letters = []rune(lower + upper)
	case AllCase:
		letters = []rune(lower + upper + digit + symbol)
	default:
		letters = []rune(lower + upper + digit)
	}

	for i := range b {
		bg := big.NewInt(int64(len(letters) - 1))
		n, _ := rand.Int(rand.Reader, bg)
		b[i] = letters[n.Int64()]
	}
	return string(b)
}
