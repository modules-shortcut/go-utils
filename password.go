package utils

import (
	"errors"
	"regexp"
)

//PasswordValidation for validate string must have uppercase, special char and number
func PasswordValidation(s string) (err error) {

	var (
		uppercaseRegex   = regexp.MustCompile(`[A-Z]`)
		lowercaseRegex   = regexp.MustCompile(`[a-z]`)
		specialCharRegex = regexp.MustCompile(`[^a-zA-Z0-9]`)
		numberRegex      = regexp.MustCompile(`[0-9]`)
	)

	// Check length
	if len(s) < 8 {
		err = errors.New("harus minimal 8 karakter")
		return
	}

	// Check for uppercase letters
	if !uppercaseRegex.MatchString(s) {
		err = errors.New("harus mengandung huruf kapital")
		return
	}

	// Check for uppercase letters
	if !lowercaseRegex.MatchString(s) {
		err = errors.New("harus mengandung huruf kecil")
		return
	}

	// Check for special characters
	if !specialCharRegex.MatchString(s) {
		err = errors.New("harus mengandung karakter spesial")
		return
	}

	// Check for numbers
	if !numberRegex.MatchString(s) {
		err = errors.New("harus mengandung angka")
		return
	}

	return
}
