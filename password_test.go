package utils

import (
	"fmt"
	"testing"
)

func TestPasswordValidation(t *testing.T) {
	tests := []struct {
		password string
		wantErr  bool
	}{
		{"Abcdef12@", false},  // Valid password
		{"abcde@fg", true},    // Missing uppercase
		{"ABCDEF12@", true},   // Missing lowercase
		{"Abcdefgh", true},    // Missing special character
		{"Abcdef123", true},   // Missing special character
		{"abcdefgh", true},    // Missing uppercase, special character, and number
		{"12345678", true},    // Missing uppercase, special character
		{"@#$%^&*(", true},    // Missing lowercase, number
		{"Abcdef12@", false},  // Valid password
		{"Abcdef12@@", false}, // Double special character
		{"Abcdef1234", true},  // Missing special character
	}

	for _, tt := range tests {
		t.Run(tt.password, func(t *testing.T) {
			err := PasswordValidation(tt.password)
			fmt.Println(err)
			if (err != nil) != tt.wantErr {
				t.Errorf("PasswordValidation() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
